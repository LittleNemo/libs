/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_math.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 15:00:56 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:33:55 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftssl_sha512.h"
#include "../../libft/incs/libft_part_sys.h"
#include "../../libftprintf/incs/libftprintf.h"

static void		sha512_math_loop(uint64_t *val, uint64_t *sin, uint64_t *word)
{
	val[8] = 0;
	while (val[8] < 80)
	{
		val[10] = val[7] + sha512_usigma1(val[4]) + sin[val[8]];
		val[10] += sha512_ch(val[4], val[5], val[6]) + word[val[8]];
		val[11] = sha512_usigma0(val[0]) + sha512_maj(val[0], val[1], val[2]);
		val[7] = val[6];
		val[6] = val[5];
		val[5] = val[4];
		val[4] = val[3] + val[10];
		val[3] = val[2];
		val[2] = val[1];
		val[1] = val[0];
		val[0] = val[10] + val[11];
		val[8]++;
	}
}

static void		sha512_set_words(uint64_t *word, uint64_t *bloc)
{
	size_t			i;

	i = 0;
	while (i < 16)
	{
		word[i] = bloc[i];
		i++;
	}
	while (i < 80)
	{
		word[i] = sha512_sigma1(word[i - 2]) + word[i - 7];
		word[i] += sha512_sigma0(word[i - 15]) + word[i - 16];
		i++;
	}
}

static uint64_t	*sha512_get_bloc(char *new_str, size_t offset)
{
	uint64_t		*bloc;
	size_t			i;

	i = 0;
	bloc = (uint64_t *)(new_str + offset);
	while (i < 16 && (ft_endian() == LITTLE_ENDIAN))
	{
		bloc[i] = ft_bswap64(bloc[i]);
		i++;
	}
	return (bloc);
}

static void		sha512_set_val(uint64_t *val, uint64_t *h)
{
	val[0] = h[0];
	val[1] = h[1];
	val[2] = h[2];
	val[3] = h[3];
	val[4] = h[4];
	val[5] = h[5];
	val[6] = h[6];
	val[7] = h[7];
}

void			sha512_math_init(uint64_t *h, char *new_str, size_t new_size)
{
	uint64_t		val[12];
	uint64_t		word[80];
	uint64_t		*bloc;
	uint64_t		*sin;
	size_t			offset;

	sin = sha512_sgl_sin();
	offset = 0;
	while (offset < new_size)
	{
		sha512_set_val(val, h);
		bloc = sha512_get_bloc(new_str, offset);
		sha512_set_words(word, bloc);
		sha512_math_loop(val, sin, word);
		h[0] += val[0];
		h[1] += val[1];
		h[2] += val[2];
		h[3] += val[3];
		h[4] += val[4];
		h[5] += val[5];
		h[6] += val[6];
		h[7] += val[7];
		offset += (1024 / 8);
	}
}
