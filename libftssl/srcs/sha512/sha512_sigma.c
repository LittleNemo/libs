/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_sigma.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 15:01:13 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:33:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../libft/incs/libft_part_sys.h"

uint64_t		sha512_sigma0(uint64_t x)
{
	return (ft_rotr64(x, 1) ^ ft_rotr64(x, 8) ^ (x >> 7));
}

uint64_t		sha512_sigma1(uint64_t x)
{
	return (ft_rotr64(x, 19) ^ ft_rotr64(x, 61) ^ (x >> 6));
}

uint64_t		sha512_usigma0(uint64_t x)
{
	return (ft_rotr64(x, 28) ^ ft_rotr64(x, 34) ^ ft_rotr64(x, 39));
}

uint64_t		sha512_usigma1(uint64_t x)
{
	return (ft_rotr64(x, 14) ^ ft_rotr64(x, 18) ^ ft_rotr64(x, 41));
}
