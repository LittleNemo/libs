/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_sgl_sin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 15:01:04 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:33:21 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static void		sha512_init_sin_00_19(uint64_t *tab)
{
	tab[0] = 0x428a2f98d728ae22;
	tab[1] = 0x7137449123ef65cd;
	tab[2] = 0xb5c0fbcfec4d3b2f;
	tab[3] = 0xe9b5dba58189dbbc;
	tab[4] = 0x3956c25bf348b538;
	tab[5] = 0x59f111f1b605d019;
	tab[6] = 0x923f82a4af194f9b;
	tab[7] = 0xab1c5ed5da6d8118;
	tab[8] = 0xd807aa98a3030242;
	tab[9] = 0x12835b0145706fbe;
	tab[10] = 0x243185be4ee4b28c;
	tab[11] = 0x550c7dc3d5ffb4e2;
	tab[12] = 0x72be5d74f27b896f;
	tab[13] = 0x80deb1fe3b1696b1;
	tab[14] = 0x9bdc06a725c71235;
	tab[15] = 0xc19bf174cf692694;
	tab[16] = 0xe49b69c19ef14ad2;
	tab[17] = 0xefbe4786384f25e3;
	tab[18] = 0x0fc19dc68b8cd5b5;
	tab[19] = 0x240ca1cc77ac9c65;
}

static void		sha512_init_sin_20_39(uint64_t *tab)
{
	tab[20] = 0x2de92c6f592b0275;
	tab[21] = 0x4a7484aa6ea6e483;
	tab[22] = 0x5cb0a9dcbd41fbd4;
	tab[23] = 0x76f988da831153b5;
	tab[24] = 0x983e5152ee66dfab;
	tab[25] = 0xa831c66d2db43210;
	tab[26] = 0xb00327c898fb213f;
	tab[27] = 0xbf597fc7beef0ee4;
	tab[28] = 0xc6e00bf33da88fc2;
	tab[29] = 0xd5a79147930aa725;
	tab[30] = 0x06ca6351e003826f;
	tab[31] = 0x142929670a0e6e70;
	tab[32] = 0x27b70a8546d22ffc;
	tab[33] = 0x2e1b21385c26c926;
	tab[34] = 0x4d2c6dfc5ac42aed;
	tab[35] = 0x53380d139d95b3df;
	tab[36] = 0x650a73548baf63de;
	tab[37] = 0x766a0abb3c77b2a8;
	tab[38] = 0x81c2c92e47edaee6;
	tab[39] = 0x92722c851482353b;
}

static void		sha512_init_sin_40_59(uint64_t *tab)
{
	tab[40] = 0xa2bfe8a14cf10364;
	tab[41] = 0xa81a664bbc423001;
	tab[42] = 0xc24b8b70d0f89791;
	tab[43] = 0xc76c51a30654be30;
	tab[44] = 0xd192e819d6ef5218;
	tab[45] = 0xd69906245565a910;
	tab[46] = 0xf40e35855771202a;
	tab[47] = 0x106aa07032bbd1b8;
	tab[48] = 0x19a4c116b8d2d0c8;
	tab[49] = 0x1e376c085141ab53;
	tab[50] = 0x2748774cdf8eeb99;
	tab[51] = 0x34b0bcb5e19b48a8;
	tab[52] = 0x391c0cb3c5c95a63;
	tab[53] = 0x4ed8aa4ae3418acb;
	tab[54] = 0x5b9cca4f7763e373;
	tab[55] = 0x682e6ff3d6b2b8a3;
	tab[56] = 0x748f82ee5defb2fc;
	tab[57] = 0x78a5636f43172f60;
	tab[58] = 0x84c87814a1f0ab72;
	tab[59] = 0x8cc702081a6439ec;
}

static void		sha512_init_sin_60_79(uint64_t *tab)
{
	tab[60] = 0x90befffa23631e28;
	tab[61] = 0xa4506cebde82bde9;
	tab[62] = 0xbef9a3f7b2c67915;
	tab[63] = 0xc67178f2e372532b;
	tab[64] = 0xca273eceea26619c;
	tab[65] = 0xd186b8c721c0c207;
	tab[66] = 0xeada7dd6cde0eb1e;
	tab[67] = 0xf57d4f7fee6ed178;
	tab[68] = 0x06f067aa72176fba;
	tab[69] = 0x0a637dc5a2c898a6;
	tab[70] = 0x113f9804bef90dae;
	tab[71] = 0x1b710b35131c471b;
	tab[72] = 0x28db77f523047d84;
	tab[73] = 0x32caab7b40c72493;
	tab[74] = 0x3c9ebe0a15c9bebc;
	tab[75] = 0x431d67c49c100d4c;
	tab[76] = 0x4cc5d4becb3e42b6;
	tab[77] = 0x597f299cfc657e2a;
	tab[78] = 0x5fcb6fab3ad6faec;
	tab[79] = 0x6c44198c4a475817;
}

uint64_t		*sha512_sgl_sin(void)
{
	static uint64_t	tab[64];

	if (!tab[0])
	{
		sha512_init_sin_00_19(tab);
		sha512_init_sin_20_39(tab);
		sha512_init_sin_40_59(tab);
		sha512_init_sin_60_79(tab);
	}
	return (tab);
}
