/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_sigma.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 15:00:00 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:29:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../libft/incs/libft_part_sys.h"

uint32_t		sha256_sigma0(uint32_t x)
{
	return (ft_rotr32(x, 7) ^ ft_rotr32(x, 18) ^ (x >> 3));
}

uint32_t		sha256_sigma1(uint32_t x)
{
	return (ft_rotr32(x, 17) ^ ft_rotr32(x, 19) ^ (x >> 10));
}

uint32_t		sha256_usigma0(uint32_t x)
{
	return (ft_rotr32(x, 2) ^ ft_rotr32(x, 13) ^ ft_rotr32(x, 22));
}

uint32_t		sha256_usigma1(uint32_t x)
{
	return (ft_rotr32(x, 6) ^ ft_rotr32(x, 11) ^ ft_rotr32(x, 25));
}
