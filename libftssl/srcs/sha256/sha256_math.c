/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_math.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:59:25 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:31:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftssl_sha256.h"
#include "../../libft/incs/libft_part_sys.h"

static void		sha256_math_loop(uint32_t *val, uint32_t *sin, uint32_t *word)
{
	val[8] = 0;
	while (val[8] < 64)
	{
		val[10] = val[7] + sha256_usigma1(val[4]) + sin[val[8]];
		val[10] += sha256_ch(val[4], val[5], val[6]) + word[val[8]];
		val[11] = sha256_usigma0(val[0]) + sha256_maj(val[0], val[1], val[2]);
		val[7] = val[6];
		val[6] = val[5];
		val[5] = val[4];
		val[4] = val[3] + val[10];
		val[3] = val[2];
		val[2] = val[1];
		val[1] = val[0];
		val[0] = val[10] + val[11];
		val[8]++;
	}
}

static void		sha256_set_words(uint32_t *word, uint32_t *bloc)
{
	size_t			i;

	i = 0;
	while (i < 16)
	{
		word[i] = bloc[i];
		i++;
	}
	while (i < 64)
	{
		word[i] = sha256_sigma1(word[i - 2]) + word[i - 7];
		word[i] += sha256_sigma0(word[i - 15]) + word[i - 16];
		i++;
	}
}

static uint32_t	*sha256_get_bloc(char *new_str, size_t offset)
{
	uint32_t		*bloc;
	size_t			i;

	i = 0;
	bloc = (uint32_t *)(new_str + offset);
	while (i < 16 && (ft_endian() == LITTLE_ENDIAN))
	{
		bloc[i] = ft_bswap32(bloc[i]);
		i++;
	}
	return (bloc);
}

static void		sha256_set_val(uint32_t *val, uint32_t *h)
{
	val[0] = h[0];
	val[1] = h[1];
	val[2] = h[2];
	val[3] = h[3];
	val[4] = h[4];
	val[5] = h[5];
	val[6] = h[6];
	val[7] = h[7];
}

void			sha256_math_init(uint32_t *h, char *new_str, size_t new_size)
{
	uint32_t		val[12];
	uint32_t		word[64];
	uint32_t		*bloc;
	uint32_t		*sin;
	size_t			offset;

	sin = sha256_sgl_sin();
	offset = 0;
	while (offset < new_size)
	{
		sha256_set_val(val, h);
		bloc = sha256_get_bloc(new_str, offset);
		sha256_set_words(word, bloc);
		sha256_math_loop(val, sin, word);
		h[0] += val[0];
		h[1] += val[1];
		h[2] += val[2];
		h[3] += val[3];
		h[4] += val[4];
		h[5] += val[5];
		h[6] += val[6];
		h[7] += val[7];
		offset += (512 / 8);
	}
}
