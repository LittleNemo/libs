/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sha256.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 15:00:19 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/15 10:12:12 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftssl_defines.h"
#include "libftssl_sha256.h"
#include "../../libft/incs/libft_part_memory.h"
#include "../../libft/incs/libft_part_string.h"
#include "../../libft/incs/libft_part_sys.h"
#include "../../libftprintf/incs/libftprintf.h"

/*
** DESCRIPTION
**	- The ssl_sha256() function generates a hash code from the given string
**	"str" of length "size" (expresed in bytes). In the first time, the
**	ssl_sha256() function computes a new size that is 448 mod 512 bits long to
**	correctly pad the original string. Next, the function generates a new string
**	with the padding and the original size wrote at the end of it. Then, the
**	math magic happens. And finally, the different parts of the hash value are
**	appended to one-another.
**
** RETURN VALUES
**	- ssl_sha256() returns a pointer to a string that represent the hash value
**	of the original message str. If an error occurs during the process, a NULL
**	pointer is returned.
*/

char			*ft_sha256(char *str, size_t size)
{
	char			*hash;
	char			*new_str;
	size_t			new_size;
	uint64_t		true_size;
	uint32_t		h[8] = {
		0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
		0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
	};

	new_size = ((((size + 8) / 64) + 1) * 64) - 8;
	if (!(new_str = (char *)ft_memalloc(new_size + 64)))
		return (NULL);
	ft_memcpy(new_str, str, size);
	new_str[size] = 0b10000000;
	true_size = size * 8;
	if (ft_endian() == LITTLE_ENDIAN)
		true_size = ft_bswap64(true_size);
	ft_memcpy(new_str + new_size, &true_size, 8);
	sha256_math_init(h, new_str, new_size);
	asprintf(&hash, RET256, h[0], h[1], h[2], h[3], h[4], h[5], h[6], h[7]);
	ft_strdel(&new_str);
	return (hash);
}
/*
**	ft_asprintf(&hash, RET256, h[0], h[1], h[2], h[3], h[4], h[5], h[6], h[7]);
*/