/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256_sgl_sin.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:59:32 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:30:01 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static void		sha256_init_sin_00_15(uint32_t *tab)
{
	tab[0] = 0x428a2f98;
	tab[1] = 0x71374491;
	tab[2] = 0xb5c0fbcf;
	tab[3] = 0xe9b5dba5;
	tab[4] = 0x3956c25b;
	tab[5] = 0x59f111f1;
	tab[6] = 0x923f82a4;
	tab[7] = 0xab1c5ed5;
	tab[8] = 0xd807aa98;
	tab[9] = 0x12835b01;
	tab[10] = 0x243185be;
	tab[11] = 0x550c7dc3;
	tab[12] = 0x72be5d74;
	tab[13] = 0x80deb1fe;
	tab[14] = 0x9bdc06a7;
	tab[15] = 0xc19bf174;
}

static void		sha256_init_sin_16_31(uint32_t *tab)
{
	tab[16] = 0xe49b69c1;
	tab[17] = 0xefbe4786;
	tab[18] = 0x0fc19dc6;
	tab[19] = 0x240ca1cc;
	tab[20] = 0x2de92c6f;
	tab[21] = 0x4a7484aa;
	tab[22] = 0x5cb0a9dc;
	tab[23] = 0x76f988da;
	tab[24] = 0x983e5152;
	tab[25] = 0xa831c66d;
	tab[26] = 0xb00327c8;
	tab[27] = 0xbf597fc7;
	tab[28] = 0xc6e00bf3;
	tab[29] = 0xd5a79147;
	tab[30] = 0x06ca6351;
	tab[31] = 0x14292967;
}

static void		sha256_init_sin_32_47(uint32_t *tab)
{
	tab[32] = 0x27b70a85;
	tab[33] = 0x2e1b2138;
	tab[34] = 0x4d2c6dfc;
	tab[35] = 0x53380d13;
	tab[36] = 0x650a7354;
	tab[37] = 0x766a0abb;
	tab[38] = 0x81c2c92e;
	tab[39] = 0x92722c85;
	tab[40] = 0xa2bfe8a1;
	tab[41] = 0xa81a664b;
	tab[42] = 0xc24b8b70;
	tab[43] = 0xc76c51a3;
	tab[44] = 0xd192e819;
	tab[45] = 0xd6990624;
	tab[46] = 0xf40e3585;
	tab[47] = 0x106aa070;
}

static void		sha256_init_sin_48_63(uint32_t *tab)
{
	tab[48] = 0x19a4c116;
	tab[49] = 0x1e376c08;
	tab[50] = 0x2748774c;
	tab[51] = 0x34b0bcb5;
	tab[52] = 0x391c0cb3;
	tab[53] = 0x4ed8aa4a;
	tab[54] = 0x5b9cca4f;
	tab[55] = 0x682e6ff3;
	tab[56] = 0x748f82ee;
	tab[57] = 0x78a5636f;
	tab[58] = 0x84c87814;
	tab[59] = 0x8cc70208;
	tab[60] = 0x90befffa;
	tab[61] = 0xa4506ceb;
	tab[62] = 0xbef9a3f7;
	tab[63] = 0xc67178f2;
}

uint32_t		*sha256_sgl_sin(void)
{
	static uint32_t	tab[64];

	if (!tab[0])
	{
		sha256_init_sin_00_15(tab);
		sha256_init_sin_16_31(tab);
		sha256_init_sin_32_47(tab);
		sha256_init_sin_48_63(tab);
	}
	return (tab);
}
