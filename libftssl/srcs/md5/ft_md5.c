/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_md5.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:58:58 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:11:07 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftssl_defines.h"
#include "libftssl_md5.h"
#include "../../libft/incs/libft_part_memory.h"
#include "../../libft/incs/libft_part_string.h"
#include "../../libft/incs/libft_part_sys.h"
#include "../../libftprintf/incs/libftprintf.h"

/*
** DESCRIPTION
**	- The ssl_md5() function generates a hash code from the given string "str"
**	of length "size" (expresed in bytes). In the first time, the ssl_md5()
**	function computes a new size that is 448 mod 512 bits long to correctly pad
**	the original string. Next, the function generates a new string with the
**	padding and the original size wrote at the end of it. Then, the math magic
**	happens. And finally, the different parts of the hash value are appended to
**	one-another.
**
** RETURN VALUES
**	- ssl_md5() returns a pointer to a string that represent the hash value of
**	the original message str. If an error occurs during the process, a NULL
**	pointer is returned.
*/

char			*ft_md5(char *str, size_t size)
{
	char			*hash;
	char			*new_str;
	size_t			new_size;
	uint32_t		true_size;
	uint32_t		h[4] = {0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476};

	new_size = ((((size + 8) / 64) + 1) * 64) - 8;
	if (!(new_str = (char *)ft_memalloc(new_size + 64)))
		return (NULL);
	ft_memcpy(new_str, str, size);
	new_str[size] = 0b10000000;
	true_size = size * 8;
	if (ft_endian() == BIG_ENDIAN)
		true_size = ft_bswap64(true_size);
	ft_memcpy(new_str + new_size, &true_size, 4);
	md5_math_init(h, new_str, new_size);
	if (ft_endian() == LITTLE_ENDIAN)
		asprintf(&hash, RETMD5, ft_bswap32(h[0]), \
		ft_bswap32(h[1]), ft_bswap32(h[2]), ft_bswap32(h[3]));
	else
		asprintf(&hash, RETMD5, h[0], h[1], h[2], h[3]);
	ft_strdel(&new_str);
	return (hash);
}
