/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_sgl_sin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:58:40 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:16:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static void		md5_init_sin_00_15(uint32_t *tab)
{
	tab[0] = 0xd76aa478;
	tab[1] = 0xe8c7b756;
	tab[2] = 0x242070db;
	tab[3] = 0xc1bdceee;
	tab[4] = 0xf57c0faf;
	tab[5] = 0x4787c62a;
	tab[6] = 0xa8304613;
	tab[7] = 0xfd469501;
	tab[8] = 0x698098d8;
	tab[9] = 0x8b44f7af;
	tab[10] = 0xffff5bb1;
	tab[11] = 0x895cd7be;
	tab[12] = 0x6b901122;
	tab[13] = 0xfd987193;
	tab[14] = 0xa679438e;
	tab[15] = 0x49b40821;
}

static void		md5_init_sin_16_31(uint32_t *tab)
{
	tab[16] = 0xf61e2562;
	tab[17] = 0xc040b340;
	tab[18] = 0x265e5a51;
	tab[19] = 0xe9b6c7aa;
	tab[20] = 0xd62f105d;
	tab[21] = 0x02441453;
	tab[22] = 0xd8a1e681;
	tab[23] = 0xe7d3fbc8;
	tab[24] = 0x21e1cde6;
	tab[25] = 0xc33707d6;
	tab[26] = 0xf4d50d87;
	tab[27] = 0x455a14ed;
	tab[28] = 0xa9e3e905;
	tab[29] = 0xfcefa3f8;
	tab[30] = 0x676f02d9;
	tab[31] = 0x8d2a4c8a;
}

static void		md5_init_sin_32_47(uint32_t *tab)
{
	tab[32] = 0xfffa3942;
	tab[33] = 0x8771f681;
	tab[34] = 0x6d9d6122;
	tab[35] = 0xfde5380c;
	tab[36] = 0xa4beea44;
	tab[37] = 0x4bdecfa9;
	tab[38] = 0xf6bb4b60;
	tab[39] = 0xbebfbc70;
	tab[40] = 0x289b7ec6;
	tab[41] = 0xeaa127fa;
	tab[42] = 0xd4ef3085;
	tab[43] = 0x04881d05;
	tab[44] = 0xd9d4d039;
	tab[45] = 0xe6db99e5;
	tab[46] = 0x1fa27cf8;
	tab[47] = 0xc4ac5665;
}

static void		md5_init_sin_48_63(uint32_t *tab)
{
	tab[48] = 0xf4292244;
	tab[49] = 0x432aff97;
	tab[50] = 0xab9423a7;
	tab[51] = 0xfc93a039;
	tab[52] = 0x655b59c3;
	tab[53] = 0x8f0ccc92;
	tab[54] = 0xffeff47d;
	tab[55] = 0x85845dd1;
	tab[56] = 0x6fa87e4f;
	tab[57] = 0xfe2ce6e0;
	tab[58] = 0xa3014314;
	tab[59] = 0x4e0811a1;
	tab[60] = 0xf7537e82;
	tab[61] = 0xbd3af235;
	tab[62] = 0x2ad7d2bb;
	tab[63] = 0xeb86d391;
}

uint32_t		*md5_sgl_sin(void)
{
	static uint32_t	tab[64];

	if (!tab[0])
	{
		md5_init_sin_00_15(tab);
		md5_init_sin_16_31(tab);
		md5_init_sin_32_47(tab);
		md5_init_sin_48_63(tab);
	}
	return (tab);
}
