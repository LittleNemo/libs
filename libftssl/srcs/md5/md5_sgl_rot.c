/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_sgl_rot.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:58:32 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:25:03 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static void		md5_init_rot_00_15(uint32_t *tab)
{
	tab[0] = 7;
	tab[1] = 12;
	tab[2] = 17;
	tab[3] = 22;
	tab[4] = 7;
	tab[5] = 12;
	tab[6] = 17;
	tab[7] = 22;
	tab[8] = 7;
	tab[9] = 12;
	tab[10] = 17;
	tab[11] = 22;
	tab[12] = 7;
	tab[13] = 12;
	tab[14] = 17;
	tab[15] = 22;
}

static void		md5_init_rot_16_31(uint32_t *tab)
{
	tab[16] = 5;
	tab[17] = 9;
	tab[18] = 14;
	tab[19] = 20;
	tab[20] = 5;
	tab[21] = 9;
	tab[22] = 14;
	tab[23] = 20;
	tab[24] = 5;
	tab[25] = 9;
	tab[26] = 14;
	tab[27] = 20;
	tab[28] = 5;
	tab[29] = 9;
	tab[30] = 14;
	tab[31] = 20;
}

static void		md5_init_rot_32_47(uint32_t *tab)
{
	tab[32] = 4;
	tab[33] = 11;
	tab[34] = 16;
	tab[35] = 23;
	tab[36] = 4;
	tab[37] = 11;
	tab[38] = 16;
	tab[39] = 23;
	tab[40] = 4;
	tab[41] = 11;
	tab[42] = 16;
	tab[43] = 23;
	tab[44] = 4;
	tab[45] = 11;
	tab[46] = 16;
	tab[47] = 23;
}

static void		md5_init_rot_48_63(uint32_t *tab)
{
	tab[48] = 6;
	tab[49] = 10;
	tab[50] = 15;
	tab[51] = 21;
	tab[52] = 6;
	tab[53] = 10;
	tab[54] = 15;
	tab[55] = 21;
	tab[56] = 6;
	tab[57] = 10;
	tab[58] = 15;
	tab[59] = 21;
	tab[60] = 6;
	tab[61] = 10;
	tab[62] = 15;
	tab[63] = 21;
}

uint32_t		*md5_sgl_rot(void)
{
	static uint32_t	tab[64];

	if (!tab[0])
	{
		md5_init_rot_00_15(tab);
		md5_init_rot_16_31(tab);
		md5_init_rot_32_47(tab);
		md5_init_rot_48_63(tab);
	}
	return (tab);
}
