/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:58:48 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:14:22 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "../../libft/incs/libft_part_math.h"
#include "../../libft/incs/libft_part_string.h"

/*
**	The md5_ltoa() function is just a custom version of the ltoa() for the md5
**	function.
*/
char			*md5_ltoa(uint32_t value)
{
	char			*str_base;
	char			*fresh;
	unsigned int	i;
	size_t			len;

	if (!(fresh = ft_strnew(8)))
		return (NULL);
	i = 0;
	len = 8;
	str_base = "0123456789abcdef";
	while (len > 0)
	{
		fresh[len-- - 1] = str_base[ft_abs(value % 16)];
		value /= 16;
	}
	return (fresh);
}

/*
**	The md5_cat_invert() function is used when concatenating the differents
**	parts of a hash in the md5 algorythme. This function looks lika a string
**	reversal function but it reverses the given string two characters at a time.
**	For exemple: the string "01234567" will be transformed into "67452301".
*/
char			*md5_cat_invert(char *str)
{
	char			*tmp;

	if (!(tmp = ft_strnew(8)))
		return (NULL);
	ft_strcpy(tmp, str);
	str[0] = tmp[6];
	str[1] = tmp[7];
	str[2] = tmp[4];
	str[3] = tmp[5];
	str[4] = tmp[2];
	str[5] = tmp[3];
	str[6] = tmp[0];
	str[7] = tmp[1];
	ft_strdel(&tmp);
	return (str);
}
