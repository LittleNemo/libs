/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftssl_defines.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:56:11 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 16:30:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSSL_DEFINES_H
# define LIBFTSSL_DEFINES_H

# define RETMD5		"%.8x%.8x%.8x%.8x"
# define RET256		"%.8x%.8x%.8x%.8x%.8x%.8x%.8x%.8x"
# define RET224		"%.8x%.8x%.8x%.8x%.8x%.8x%.8x"
# define RET512		"%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx"
# define RET384		"%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx"
# define RETWRP		"%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx%.16llx"

#endif