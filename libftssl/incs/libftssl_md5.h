/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftssl_md5.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:56:24 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/15 10:10:31 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSSL_MD5_H
# define LIBFTSSL_MD5_H

# include <stdlib.h>

char			*ft_md5(char *str, size_t size);
char			*md5_cat_invert(char *str);
char			*md5_ltoa(uint32_t value);
void			md5_math_init(uint32_t *h, char *new_str, size_t new_size);
uint32_t		*md5_sgl_rot(void);
uint32_t		*md5_sgl_sin(void);

#endif