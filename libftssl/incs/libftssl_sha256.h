/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftssl_sha256.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:56:34 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/15 10:11:32 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSSL_SHA256_H
# define LIBFTSSL_SHA256_H

# include <stdlib.h>

char			*ft_sha256(char *str, size_t size);
char			*ft_sha224(char *str, size_t size);
void			sha256_math_init(uint32_t *h, char *new_str, size_t new_size);
uint32_t		sha256_ch(uint32_t x, uint32_t y, uint32_t z);
uint32_t		sha256_maj(uint32_t x, uint32_t y, uint32_t z);
uint32_t		sha256_sigma0(uint32_t x);
uint32_t		sha256_sigma1(uint32_t x);
uint32_t		sha256_usigma0(uint32_t x);
uint32_t		sha256_usigma1(uint32_t x);
uint32_t		*sha256_sgl_sin(void);

#endif