/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftssl_sha512.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/30 14:56:46 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/15 10:11:42 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSSL_SHA512_H
# define LIBFTSSL_SHA512_H

# include <stdlib.h>

char			*ft_sha512(char *str, size_t size);
char			*ft_sha384(char *str, size_t size);
void			sha512_math_init(uint64_t *h, char *new_str, size_t new_size);
uint64_t		sha512_ch(uint64_t x, uint64_t y, uint64_t z);
uint64_t		sha512_maj(uint64_t x, uint64_t y, uint64_t z);
uint64_t		sha512_sigma0(uint64_t x);
uint64_t		sha512_sigma1(uint64_t x);
uint64_t		sha512_usigma0(uint64_t x);
uint64_t		sha512_usigma1(uint64_t x);
uint64_t		*sha512_sgl_sin(void);

#endif