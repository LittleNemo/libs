/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftssl_typedefs.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/23 16:28:00 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 16:38:50 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSSL_TYPEDEFS_H
# define LIBFTSSL_TYPEDEFS_H

# include <stdlib.h>

typedef struct	s_file {
	int				fd;
	char			*name;
	char			*hash;
	void			*content;
	size_t			size;
}				t_file;

#endif
