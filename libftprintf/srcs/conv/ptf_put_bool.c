/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ptf_put_bool.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/30 14:57:27 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 15:19:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "libftprintf_part_utils.h"
#include "libftprintf_part_write.h"
#include "libftprintf_defines.h"
#include "libftprintf_typedefs.h"

static void		ptf_get_bool(t_flags *flags, t_conv *b, va_list ap)
{
	b->n_tmp = va_arg(ap, int);
	b->s_str = "true";
	if (!b->n_tmp)
		b->s_str = "false";
	b->fill = ' ';
	if ((flags->p_flags & PRINTF_F_ZERO) && !(flags->p_flags & PRINTF_F_MINU))
		b->fill = '0';
	b->nlen = ptf_strlen(b->s_str);
	b->flen = 0;
	if (flags->field > b->nlen)
		b->flen = flags->field - b->nlen;
}

void			ptf_put_bool(t_flags *flags, t_buff *buff, va_list ap)
{
	t_conv			b;

	ptf_get_bool(flags, &b, ap);
	if (flags->p_flags & PRINTF_F_MINU)
	{
		ptf_buff_add_str(buff, b.s_str++, b.nlen);
		while (b.flen--)
			ptf_buff_add_char(buff, &b.fill);
		return ;
	}
	while (b.flen--)
		ptf_buff_add_char(buff, &b.fill);
	ptf_buff_add_str(buff, b.s_str++, b.nlen);
}
