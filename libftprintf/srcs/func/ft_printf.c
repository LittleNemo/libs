/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 14:07:24 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/14 18:07:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include "libftprintf.h"
#include "libftprintf_part_conv.h"
#include "libftprintf_part_singl.h"
#include "libftprintf_part_write.h"
#include "libftprintf_typedefs.h"

int				ft_printf(const char *format, ...)
{
	int				ret;
	va_list			ap;

	if (!format)
		return (-1);
	va_start(ap, format);
	ret = ft_vprintf(format, ap);
	va_end(ap);
	return (ret);
}

int				ft_vprintf(const char *format, va_list ap)
{
	t_buff			buff;
	t_flags			flags;
	t_convert		tab;

	ptf_buff_init(&buff, stdout->_file);
	tab = ptf_sgl_hash();
	while (*format)
	{
		if (*format == '%')
		{
			format = ptf_get_flags(&flags, ++format, ap);
			if (flags.type >= '@' && flags.type <= 'x')
				(*tab[flags.type - '@'])(&flags, &buff, ap);
			else if (flags.type)
				ptf_put_char(&flags, &buff, ap);
		}
		else
			ptf_buff_add_char(&buff, format++);
	}
	return (buff.ret += write(buff.fd, buff.str, buff.index));
}
