/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 12:58:24 by lbrangie          #+#    #+#             */
/*   Updated: 2020/02/04 14:21:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include "libftprintf.h"
#include "libftprintf_part_conv.h"
#include "libftprintf_part_singl.h"
#include "libftprintf_part_write.h"
#include "libftprintf_typedefs.h"

int				ft_fprintf(FILE *stream, const char *format, ...)
{
	int				ret;
	va_list			ap;

	if (!format)
		return (-1);
	va_start(ap, format);
	ret = ft_vfprintf(stream, format, ap);
	va_end(ap);
	return (ret);
}

int				ft_vfprintf(FILE *stream, const char *format, va_list ap)
{
	t_buff			buff;
	t_flags			flags;
	t_convert		tab;

	ptf_buff_init(&buff, stream->_file);
	tab = ptf_sgl_hash();
	while (*format)
	{
		if (*format == '%')
		{
			format = ptf_get_flags(&flags, ++format, ap);
			if (flags.type >= '@' && flags.type <= 'x')
				(*tab[flags.type - '@'])(&flags, &buff, ap);
			else if (flags.type)
				ptf_put_char(&flags, &buff, ap);
		}
		else
			ptf_buff_add_char(&buff, format++);
	}
	return (buff.ret += write(buff.fd, buff.str, buff.index));
}
