/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_asprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 15:45:21 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:47:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include "libftprintf.h"
#include "libftprintf_part_conv.h"
#include "libftprintf_part_singl.h"
#include "libftprintf_part_write.h"
#include "libftprintf_typedefs.h"

int				ft_asprintf(char **str, const char *format, ...)
{
	int				ret;
	va_list			ap;

	if (!format)
		return (-1);
	va_start(ap, format);
	ret = ft_vasprintf(str, format, ap);
	va_end(ap);
	return (ret);
}

int				ft_vasprintf(char **str, const char *format, va_list ap)
{
	// WORK IN PROGRESS
	(void)str;
	(void)format;
	(void)ap;
	return (0);
}
