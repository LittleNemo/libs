/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_snprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/04 13:09:07 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:47:40 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include "libftprintf.h"
#include "libftprintf_part_conv.h"
#include "libftprintf_part_singl.h"
#include "libftprintf_part_write.h"
#include "libftprintf_typedefs.h"

int				ft_snprintf(char *str, size_t size, const char *format, ...)
{
	int				ret;
	va_list			ap;

	if (!format)
		return (-1);
	va_start(ap, format);
	ret = ft_vsnprintf(str, size, format, ap);
	va_end(ap);
	return (ret);
}

int				ft_vsnprintf(char *str, size_t size, const char *format, va_list ap)
{
	// WORK IN PROGRESS
	(void)str;
	(void)size;
	(void)format;
	(void)ap;
	return (0);
}
