/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf_part_conv.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2020/02/04 16:32:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_PART_CONV_H
# define LIBFTPRINTF_PART_CONV_H

# include <stdarg.h>
# include "libftprintf_typedefs.h"

const char		*ptf_get_flags(t_flags *flags, const char *format, va_list ap);

void			ptf_put_str(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_wstr(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ptr(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_dec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ldec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_oct(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_loct(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_udec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ludec(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_hex(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_mhex(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_char(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_wchar(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_bin(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_arbs(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_ret(t_flags *flags, t_buff *buff, va_list ap);
void			ptf_put_bool(t_flags *flags, t_buff *buff, va_list ap);

uintmax_t		ptf_select_cast(t_flags *flags, va_list ap);
uintmax_t		ptf_select_ucast(t_flags *flags, va_list ap);

#endif