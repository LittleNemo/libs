/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf_part_utils.h                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2020/02/04 16:32:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_PART_UTILS_H
# define LIBFTPRINTF_PART_UTILS_H

# include <stdlib.h>

long long		ptf_abs(long long n);
int				ptf_isalpha(int c);
int				ptf_isdigit(int c);
int				ptf_isflag(char c);

size_t			ptf_strlen(const char *s);
int				ptf_wcharlen(wchar_t wchar);
int				ptf_wstrlen(wchar_t *wstr);
size_t			ptf_numlen_base(intmax_t n, int base);
size_t			ptf_numlen_ubase(uintmax_t n, int base);

#endif