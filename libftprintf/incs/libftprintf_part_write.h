/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf_part_write.h                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2020/02/04 16:32:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_PART_WRITE_H
# define LIBFTPRINTF_PART_WRITE_H

# include "libftprintf_typedefs.h"

void			ptf_buff_add_char(t_buff *buff, const char *format);
void			ptf_buff_add_uchar(t_buff *buff, unsigned char wchar);
void			ptf_buff_add_str(t_buff *buff, char *add, unsigned int len);
void			ptf_buff_flush(t_buff *buff);
void			ptf_buff_init(t_buff *buff, int fd);

void			ptf_lltoa_base(t_flags *flags, t_conv *d, int base);

void			ptf_write_num(t_flags *flags, t_buff *buff, t_conv *num);

#endif