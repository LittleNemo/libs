/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf_part_singl.h                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/08 13:41:14 by lbrangie          #+#    #+#             */
/*   Updated: 2020/02/04 16:32:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_PART_SINGL_H
# define LIBFTPRINTF_PART_SINGL_H

# include "libftprintf_typedefs.h"

char			**ptf_sgl_colors(void);
t_convert		ptf_sgl_hash(void);

#endif