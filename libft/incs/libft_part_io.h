/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_io.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:33:26 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/02 17:09:16 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_IO_H
# define LIBFT_PART_IO_H

# include "libft_typedefs.h"

/*
**	These functions are part of the io portion of the libft
*/
int				ft_putarr(char **arr);
int				ft_putarr_fd(const int fd, char **arr);
int				ft_putchar(char c);
int				ft_putchar_endl(char c);
int				ft_putchar_endl_fd(const int fd, char c);
int				ft_putchar_fd(const int fd, char c);
int				ft_putfile(const char *file_name);
int				ft_putfile_fd(const int fd, const char *file_name);
int				ft_putstr(const char *s);
int				ft_putstr_endl(const char *s);
int				ft_putstr_endl_fd(const int fd, const char *s);
int				ft_putstr_fd(const int fd, const char *s);

int				get_next_line(const int fd, char **line, t_bool nl);

#endif
