/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_typedefs.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 16:01:01 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 16:49:12 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_TYPEDEFS_H
# define LIBFT_TYPEDEFS_H

# include <stdlib.h>

typedef struct	s_dll	{
	size_t			size;
	struct s_link	*head;
	struct s_link	*tail;
}				t_dll;

typedef struct	s_link	{
	size_t			id;
	void			*content;
	struct s_dll	*header;
	struct s_link	*next;
	struct s_link	*prev;
}				t_link;

# ifndef __STDBOOL_H

typedef enum	e_bool	{
	error = -1,
	false = 0,
	true = 1
}				t_bool;

# endif

#endif
