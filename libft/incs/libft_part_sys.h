/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_sys.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:35:08 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 12:48:26 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_SYS_H
# define LIBFT_PART_SYS_H

/*
**	These functions are part of the sys portion of the libft
*/
short			ft_endian(void);
uint16_t		ft_bswap16(uint16_t x);
uint32_t		ft_bswap32(uint32_t x);
uint64_t		ft_bswap64(uint64_t x);
uint64_t		ft_rotl8(uint64_t num, unsigned int rotation);
uint64_t		ft_rotl16(uint64_t num, unsigned int rotation);
uint64_t		ft_rotl32(uint64_t num, unsigned int rotation);
uint64_t		ft_rotl64(uint64_t num, unsigned int rotation);
uint64_t		ft_rotr8(uint64_t num, unsigned int rotation);
uint64_t		ft_rotr16(uint64_t num, unsigned int rotation);
uint64_t		ft_rotr32(uint64_t num, unsigned int rotation);
uint64_t		ft_rotr64(uint64_t num, unsigned int rotation);

#endif
