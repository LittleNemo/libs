/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_memory.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:36:26 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/13 09:52:53 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_MEMORY_H
# define LIBFT_PART_MEMORY_H

# include <stdlib.h>
# include "libft_typedefs.h"

/*
**	These functions are part of the memory portion of the libft
*/
int				ft_memcmp(const void *s1, const void *s2, size_t n);
t_bool			ft_memdel(void **ap);
void			ft_bzero(void *s, size_t n);
void			*ft_memalloc(size_t size);
void			*ft_memcat(void *dst, size_t d_len, void *src, size_t s_len);
void			*ft_memccpy(void *dst, const void *src, int c, size_t n);
void			*ft_memchr(const void *s, int c, size_t n);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memjoin(void *p1, size_t size1, void *p2, size_t size2);
void			*ft_memmove(void *dst, const void *src, size_t len);
void			*ft_memset(void *b, int c, size_t len);

#endif
