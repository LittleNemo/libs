/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_part_list.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/18 15:35:08 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 12:48:26 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PART_LIST_H
# define LIBFT_PART_LIST_H

# include "libft_typedefs.h"

/*
**	These functions are part of the list portion of the libft
*/
t_bool			ft_dllpush_head(t_dll *header, void *content);
t_bool			ft_dllpush_tail(t_dll *header, void *content);
t_link			*ft_dllpop_head(t_dll *header);
t_link			*ft_dllpop_tail(t_dll *header);
void			ft_dlldel(t_dll *header, void (*del)(void *content));
void			ft_dlliter_back(t_dll *header, void (*f)(void *content));
void			ft_dlliter_fore(t_dll *header, void (*f)(void *content));

#endif
