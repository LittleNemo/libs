/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_defines.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 15:56:50 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 16:30:57 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_DEFINES_H
# define LIBFT_DEFINES_H

# define STR_XDIGIT_MAJ		"0123456789ABCDEF"
# define STR_XDIGIT_MIN		"0123456789abcdef"
# define BUFF_SIZE			2048
# define BIG_BUFF_SIZE		52428800

#endif