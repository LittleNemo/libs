/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrmrg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 15:12:56 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 15:44:54 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_arrmrg() function allocates an null-terminated string and fills it
**	with the entries of the array arr, separeted by a space (' ').
**
** RETURN VALUES
**	- ft_arrmrg() returns a pointer to the allocated string, or NULL if an
**	error occured.
*/

char			*ft_arrmrg(char **arr)
{
	char			*str;
	size_t			len;
	size_t			i;

	if (!arr)
		return (NULL);
	len = 0;
	i = 0;
	while (arr[i])
		len += ft_strlen(arr[i++]) + 1;
	if (!(str = ft_strnew(len)))
		return (NULL);
	i = 0;
	while (arr[i])
	{
		ft_strcat(str, arr[i++]);
		ft_strcat(str, " ");
	}
	return (str);
}
