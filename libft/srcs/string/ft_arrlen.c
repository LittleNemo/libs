/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/11 16:37:24 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:40:03 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_arrlen() function computes the length of the array arr.
**
** RETURN VALUES
**	- ft_arrlen() returns the number of rows that precedd the terminating NULL
**	pointer.
*/

size_t			ft_arrlen(char **arr)
{
	size_t			size;

	size = 0;
	while (arr[size])
		size++;
	return (size);
}
