/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arrdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/13 10:23:02 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/26 17:02:53 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_typedefs.h"

/*
** DESCRIPTION
**	- The ft_arrdel() frees the memory allocated for every address of the array
**	arr, then free the memory of the string.
**
** RETURN VALUE
**	- ft_arrdel() returns true if the array has succesfully be deleted.
**	Otherwise, false is returned.
*/

t_bool			ft_arrdel(void **arr, size_t len)
{
	size_t			i;

	if (!arr)
		return (false);
	i = 0;
	while (i < len)
		free(arr[i++]);
	free(arr);
	return (true);
}
