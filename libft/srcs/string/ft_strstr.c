/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 15:40:06 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:29:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strstr() function locates the first occurence of the
**	null-terminated string needle in the null-terminated string hay.
**
** RETURN VALUES
**	- ft_strstr() returns hay if needle is an empty string, NULL if needle
**	occurs nowhere in hay, otherwise, a pointer to the first character of
**	the first occurence of needle.
*/

char			*ft_strstr(const char *hay, const char *needle)
{
	size_t			size;

	if (!*needle)
		return ((char *)hay);
	size = ft_strlen(needle);
	while ((hay = ft_strchr(hay, *needle)))
		if (!ft_strncmp(hay++, needle, size))
			return ((char *)--hay);
	return (NULL);
}
