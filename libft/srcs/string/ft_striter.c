/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 17:02:04 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:23:35 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_typedefs.h"
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_striter() function applies the f function on every characters of
**	the strinfg s.
**
** RETURN VALUE
**	- ft_striter() returns true if f has been applied succesfully. Otherwise,
**	false is returned.
*/

t_bool			ft_striter(char *s, void (*f)(char *))
{
	if (!s || !f)
		return (false);
	while (*s)
		f(s++);
	return (true);
}
