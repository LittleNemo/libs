/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcasestr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 19:03:36 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 19:17:51 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_strcasestr() function locates the first occurence of the
**	null-terminated string needle in the null-terminated string hay, ignoring
**	the case.
**
** RETURN VALUES
**	- ft_strcasestr() returns hay if needle is an empty string, NULL if needle
**	occurs nowhere in hay, otherwise, a pointer to the first character of the
**	first occurence of needle.
*/

char			*ft_strcasestr(const char *hay, const char *needle)
{
	size_t			size;

	if (!*needle)
		return ((char *)hay);
	size = ft_strlen(needle);
	while ((hay = ft_strcasechr(hay, *needle)))
		if (!ft_strncasecmp(hay++, needle, size))
			return ((char *)--hay);
	return (NULL);
}
