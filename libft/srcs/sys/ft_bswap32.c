/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bswap32.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 11:35:22 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:44:40 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_bswap32() function swaps the bits of an uint32_t variable 8 to 8.
**	This is used to change a value from big endian to little endian or the other
**	way around.
**	Example:
**		bswap64 of the value
**			0b11111111000000001111111100000000
**		result to
**			0b00000000111111110000000011111111
**
** RETURN VALUES
**	- ft_bswap32() returns the value swaped.
*/

uint32_t		ft_bswap32(uint32_t x)
{
	return ((((x & 0xff000000) >> 24) | ((x & 0x00ff0000) >> 8) | \
		((x & 0x0000ff00) << 8) | ((x & 0x000000ff) << 24)));
}
