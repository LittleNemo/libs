/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotr32.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 12:09:41 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:43:11 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_rotr32() function computes the right shifting of the num value by
**	rotation amount. ft_rotr32() works with a value of 32 bits lenght.
**	Example:
**		right rotate 0b01000001 by 2 will output 0b00010000
**
** RETURN VALUES
**	- ft_rot_right() returns the comptated rotation of num.
*/

uint64_t		ft_rotr32(uint64_t num, unsigned int rotation)
{
	return ((num >> rotation) | (num << (32 - rotation)));
}
