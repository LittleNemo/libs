/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_endian.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 13:10:46 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:45:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_endian() function tests if the machine works in little or big
**	endian.
**
** RETURN VALUES
**	- ft_endian() returns the value defined by LITTLE ENDIAN if the machine is
**	in little endian and the value defined by BIG ENDIAN otherwise.
*/

short			ft_endian(void)
{
	short int		word;
	char			*b;

	word = 1;
	b = (char *)&word;
	if (b[0])
		return (LITTLE_ENDIAN);
	return (BIG_ENDIAN);
}
