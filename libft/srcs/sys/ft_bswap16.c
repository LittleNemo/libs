/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bswap16.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 11:35:22 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:44:24 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_bswap16() function swaps the bits of an uint16_t variable 8 to 8.
**	This is used to change a value from big endian to little endian or the other
**	way around.
**	Example:
**		bswap64 of the value
**			0b1111111100000000
**		result to
**			0b0000000011111111
**
** RETURN VALUES
**	- ft_bswap16() returns the value swaped.
*/

uint16_t		ft_bswap16(uint16_t x)
{
	return ((((x >> 8) & 0xff) | ((x & 0xff) << 8)));
}
