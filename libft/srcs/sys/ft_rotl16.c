/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rotl16.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/30 12:09:41 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:43:57 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_rotl16() function computes the left shifting of the num value by
**	rotation amount. ft_rotl16() works with a value of 16 bits lenght.
**	Example:
**		left rotate 0b00000010 by 2 will output 0b00001000
**
** RETURN VALUES
**	- ft_rot_left() returns the comptated rotation of num.
*/

uint64_t		ft_rotl16(uint64_t num, unsigned int rotation)
{
	return ((num << rotation) | (num >> (16 - rotation)));
}
