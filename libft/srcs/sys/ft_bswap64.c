/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bswap64.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/28 11:35:22 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:44:46 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** DESCRIPTION
**	- The ft_bswap64() function swaps the bits of an uint64_t variable 8 to 8.
**	This is used to change a value from big endian to little endian or the other
**	way around.
**	Example:
**		bswap64 of the value
**			0b1111111100000000111111110000000011111111000000001111111100000000
**		result to
**			0b0000000011111111000000001111111100000000111111110000000011111111
**
** RETURN VALUES
**	- ft_bswap64() returns the value swaped.
*/

uint64_t		ft_bswap64(uint64_t x)
{
	return ((((x & 0xff00000000000000ull) >> 56) | \
		((x & 0x00ff000000000000ull) >> 40) | \
		((x & 0x0000ff0000000000ull) >> 24) | \
		((x & 0x000000ff00000000ull) >> 8) | \
		((x & 0x00000000ff000000ull) << 8) | \
		((x & 0x0000000000ff0000ull) << 24) | \
		((x & 0x000000000000ff00ull) << 40) | \
		((x & 0x00000000000000ffull) << 56)));
}
