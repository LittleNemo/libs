/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 09:41:13 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:39:04 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_memory.h"

/*
** DESCRIPTION
**	- The ft_memcat() function appends a copy of the allocated memory space src
**	of length s_len to the memory space dst of length d_len.
**
** RETURN VALUES
**	- ft_memcat() returns a pointer to the void pointer to dst.
*/

void			*ft_memcat(void *dst, size_t d_len, void *src, size_t s_len)
{
	ft_memcpy(dst + d_len, src, s_len);
	return (dst);
}
