/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/13 09:51:39 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:38:52 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_memory.h"

/*
** DESCRIPTION
**	- The ft_memjoin() function allocates memory to contain both the contents of
**	pointers p1 and p2, of respective sizes size1 and size2, copies p1 into this
**	fresh memory space and appends p2 to the end of fresh.
**
** RETURN VALUES
**	- ft_memjoin() returns a pointer to the fresh memory space. If an error
**	occured, a null-pointer is return instead.
*/

void			*ft_memjoin(void *p1, size_t size1, void *p2, size_t size2)
{
	void			*fresh;

	if (!p1 || ! p2 || !(fresh = ft_memalloc(size1 + size2)))
		return (NULL);
	ft_memcpy(fresh, p1, size1);
	ft_memcat(fresh, size1, p2, size2);
	return (fresh);
}
