/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putarr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 16:03:52 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 13:33:58 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_putarr_fd() function attempts to write the content of the array arr
**	to the object referenced by the descriptor fd.
**
** RETURN VALUES
**	- ft_putarr_fd() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putarr_fd(const int fd, char **arr)
{
	size_t			i;
	int				len;

	if (!arr)
		return (-1);
	i = 0;
	len = 0;
	while (arr[i])
		len += ft_putstr_endl_fd(fd, arr[i++]);
	return (len);
}
