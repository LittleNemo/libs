/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 13:57:24 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 13:29:13 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_putstr_fd() function attempts to write the string s to the object
**	referenced by the descriptor fd.
**
** RETURN VALUES
**	- ft_putstr_fd() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putstr_fd(const int fd, char const *s)
{
	return (write(fd, s, ft_strlen(s)));
}
