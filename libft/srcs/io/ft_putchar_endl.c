/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_endl.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 15:35:37 by lbrangie          #+#    #+#             */
/*   Updated: 2019/03/19 17:16:17 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** DESCRIPTION
**	- The ft_putchar_endl() function attempts to write the character c followed
**	by a '\n' character on the standard output, a.k.a. stdout.
**
** RETURN VALUES
**	- ft_putchar_endl() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putchar_endl(char c)
{
	return (write(1, &c, 1) + write(1, "\n", 1));
}
