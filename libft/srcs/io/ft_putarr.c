/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putarr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 15:38:06 by lbrangie          #+#    #+#             */
/*   Updated: 2019/04/26 16:56:09 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft_part_io.h"

/*
** DESCRIPTION
**	- The ft_putarr() function attempts to write the content of the array arr on
**	the standard output, a.k.a. sdtout.
**
** RETURN VALUES
**	- ft_putarr() returns the number of bytes writen upon successful completion.
**	Otherwise, -1 is returned.
*/

int				ft_putarr(char **arr)
{
	size_t			i;
	int				len;

	if (!arr)
		return (-1);
	i = 0;
	len = 0;
	while (arr[i])
		len += ft_putstr_endl(arr[i++]);
	return (len);
}
