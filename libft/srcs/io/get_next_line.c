/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 14:50:35 by lbrangie          #+#    #+#             */
/*   Updated: 2021/09/06 16:36:31 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <limits.h>
#include "libft_defines.h"
#include "libft_typedefs.h"
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The get_next_line() function reads the content of the file descriptor "fd"
**	and set the value of the pointer "*line" to the next line of the file. A
**	line is concidered to be a string of characters ended by a newline "\n". The
**	newline character is added to the pointer "*line" if the t_bool variable
**	"nl" is set to true, otherwise, it is not. The get_next_line() function can
**	keep track of the state of as many files as the defined value of OPEN_MAX is
**	set to, meaning it can read the next line of the file A, then the line of
**	the file B and then the file A again.
**	- The gnl_reader() function reads the file if no newline character is found
**	in the "memory of the file".
**
** RETURN VALUES
**	- get_next_line() returns 1 when a line is readed, 0 when the last line of
**	the file is readed and -1 is any error occured during the process
**
*/

static int		gnl_reader(const int fd, char **tab)
{
	char			*buff;
	char			*tmp;
	int				ret;

	if (!(buff = ft_strnew(BUFF_SIZE)))
		return (-1);
	ret = 1;
	while (!ft_strchr(tab[fd], '\n') && ((ret = read(fd, buff, BUFF_SIZE)) > 0))
	{
		buff[ret] = '\0';
		tmp = tab[fd];
		if (!(tab[fd] = ft_strjoin(tmp, buff)))
			return (-1);
		ft_strdel(&tmp);
	}
	ft_strdel(&buff);
	return (ret);
}

int				get_next_line(const int fd, char **line, t_bool nl)
{
	static char		*tab[OPEN_MAX];
	char			*tmp;
	int				ret;
	size_t			len;

	if ((fd < 0) || (fd > OPEN_MAX) || (line == NULL) || \
		(tab[fd] == NULL && !(tab[fd] = ft_strnew(0))) || \
		(ret = gnl_reader(fd, tab)) == -1)
		return (-1);
	if (ret == 0)
	{
		if (!(*line = ft_strdup(tab[fd])))
			return (-1);
		ft_strdel(&tab[fd]);
		return (!!ft_strlen(*line));
	}
	len = ft_strchr(tab[fd], '\n') - tab[fd] + nl;
	if (!(*line = ft_strsub(tab[fd], 0, len)))
		return (-1);
	tmp = tab[fd];
	if (!(tab[fd] = ft_strdup(ft_strchr(tmp, '\n') + 1)))
		return (-1);
	ft_strdel(&tmp);
	return (1);
}
