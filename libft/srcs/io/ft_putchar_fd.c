/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/13 13:54:33 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 13:24:47 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** DESCRIPTION
**	- The ft_putchar_fd() function attempts to write the character c to the
**	object referenced by the descriptor fd.
**
** RETURN VALUES
**	- ft_putchar_fd() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putchar_fd(const int fd, char c)
{
	return (write(fd, &c, 1));
}
