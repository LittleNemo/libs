/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_endl_fd.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 16:02:15 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 13:28:49 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft_part_string.h"

/*
** DESCRIPTION
**	- The ft_putstr_endl_fd() function attempts to write the string s followed
**	by a '\n' character to the object referenced by the descriptor fd.
**
** RETURN VALUES
**	- ft_putstr_endl_fd() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putstr_endl_fd(const int fd, const char *s)
{
	return (write(fd, s, ft_strlen(s)) + write(fd, "\n", 1));
}
