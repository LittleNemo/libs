/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_endl_fd.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/16 15:41:26 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 13:24:10 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** DESCRIPTION
**	- The ft_putchar_endl_fd() function attempts to write the character c
**	followed by a '\n' character to the object referenced by the descriptor fd.
**
** RETURN VALUES
**	- ft_putchar_endl_fd() returns the number of bytes writen upon successful
**	completion. Otherwise, -1 is returned.
*/

int				ft_putchar_endl_fd(const int fd, char c)
{
	return (write(fd, &c, 1) + write(fd, "\n", 1));
}
