/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dllpush_head.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 13:10:46 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 13:47:39 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_typedefs.h"
#include "libft_part_memory.h"

/*
** DESCRIPTION
**	- The ft_dllpush_head() function allocates the memory for a structure t_link
**	and links the content into it. Then, this new link is pushed at the start of
**	the doubly linked list refered by header.
**
** RETURN VALUES
**	- ft_dllpush_head() returns true if the link has been succesfully pushed
**	into the list and false if an error occured.
**
** NOTE
**	- See libft_defines.h to see the t_dll and t_link structures.
*/

t_bool			ft_dllpush_head(t_dll *header, void *content)
{
	t_link			*fresh;

	if (!content || !(fresh = (t_link *)ft_memalloc(sizeof(*fresh))))
		return (false);
	fresh->id = header->size++;
	fresh->content = content;
	fresh->header = header;
	if (!header->head)
	{
		header->head = fresh;
		header->tail = fresh;
		return (true);
	}
	fresh->next = header->head;
	header->head->prev = fresh;
	header->head = fresh;
	return (true);
}
