/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlliter_fore.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 14:22:29 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 13:06:47 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_typedefs.h"

/*
** DESCRIPTION
**	- The ft_dlliter_fore() function qpplies the function pointed by f on each
**	links of the doubly linked list refered by header starting from the head of
**	the list.
**
** NOTE
**	- See libft_defines.h to see the t_dll and t_link structures.
*/

void			ft_dlliter_fore(t_dll *header, void (*f)(void *content))
{
	t_link			*tmp;

	if (!header || !f)
		return ;
	tmp = header->head;
	while (tmp)
	{
		f(tmp->content);
		tmp = tmp->next;
	}
}
