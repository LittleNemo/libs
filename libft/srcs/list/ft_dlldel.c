/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlldel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 17:09:20 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 13:01:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_typedefs.h"

/*
** DESCRIPTION
**	- The ft_dlldel() function qpplies the function pointed by del on each links
**	of the doubly linked list refered by header and frees each of this links.
**
** NOTE
**	- See libft_defines.h to see the t_dll and t_link structures.
*/

void			ft_dlldel(t_dll *header, void (*del)(void *content))
{
	t_link			*tmp;

	if (!header || !del)
		return ;
	while (header->head)
	{
		tmp = header->head;
		header->head = header->head->next;
		del(tmp->content);
		free(tmp);
	}
}
