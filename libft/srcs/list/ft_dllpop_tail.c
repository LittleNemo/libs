/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dllpop_tail.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 16:46:53 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 13:46:44 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_typedefs.h"

/*
** DESCRIPTION
**	- The ft_dllpop_tail() function unlinks the last link of the doubly linked
**	list refered by header and returns it.
**
** RETURN VALUES
**	- ft_dllpop_tail() returns a pointer to a t_link structure.
**
** NOTE
**	- See libft_defines.h to see the t_dll and t_link structures.
*/

t_link			*ft_dllpop_tail(t_dll *header)
{
	t_link			*tail;

	tail = header->tail;
	if (header->size >= 2)
		header->tail->prev->next = NULL;
	if (header->size == 1)
		header->head = NULL;
	header->tail = header->tail->prev;
	tail->prev = NULL;
	header->size--;
	return (tail);
}
