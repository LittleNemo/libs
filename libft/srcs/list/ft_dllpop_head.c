/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dllpop_head.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 16:33:18 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/30 13:46:29 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_typedefs.h"

/*
** DESCRIPTION
**	- The ft_dllpop_head() function unlinks the first link of the doubly linked
**	list refered by header and returns it.
**
** RETURN VALUES
**	- ft_dllpop_head() returns a pointer to a t_link structure.
**
** NOTE
**	- See libft_defines.h to see the t_dll and t_link structures.
*/

t_link			*ft_dllpop_head(t_dll *header)
{
	t_link			*head;

	head = header->head;
	if (header->size >= 2)
		header->head->next->prev = NULL;
	if (header->size == 1)
		header->tail = NULL;
	header->head = header->head->next;
	head->next = NULL;
	header->size--;
	return (head);
}
