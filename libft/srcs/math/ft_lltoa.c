/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lltoa.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 10:39:21 by lbrangie          #+#    #+#             */
/*   Updated: 2022/08/23 17:38:00 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft_part_math.h"

/*
** DESCRIPTION
**	- The ft_lltoa() function converts the n long in a null-terminated string
**	that has been allocated. Negative numbers are handled.
**
** RETURN VALUES
**	- ft_lltoa() returns the string that has been allocated. If an error
**	happened, a NULL pointer is returned.
*/

char			*ft_lltoa(long n)
{
	return (ft_lltoa_base(n, 10));
}
