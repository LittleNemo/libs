/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isarmstrong.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lbrangie <lbrangie@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/23 15:47:00 by lbrangie          #+#    #+#             */
/*   Updated: 2020/01/23 16:50:27 by lbrangie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft_part_math.h"

/*
** DESCRIPTION
**	- The ft_isarmstrong() function tests if the integer n is an armstrong
**	number. An armstrong number is a number that equals the sum of each of its
**	digits to the power of the length of this number.
**
**	RETURN VALUES
**	- ft_isarmstrong() returns 1 if the test is true or 0 if the test is false.
**
**	EXEMPLE
**	- The smallest exemple of an armstrong number (other than the 1-digit
**	numbers) is 153 = 1^3 + 5^3 + 3^3
*/

int				ft_isarmstrong(int n)
{
	int				dup;
	int				sum;
	size_t			len;

	dup = n;
	sum = 0;
	len = ft_numlen(n);
	while (dup)
	{
		sum += ft_pow(dup % 10, len);
		dup /= 10;
	}
	return (n == sum);
}
